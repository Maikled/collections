﻿using System;
using System.Collections;
using System.Collections.Generic;
using Collections.Interfaces;
using System.Linq;

namespace Collections
{
	public class DynamicArray<T> : IDynamicArray<T>
	{
		private T[] mass;

		public DynamicArray()
		{
			mass = new T[8];
			Capacity = 8;
		}

		public DynamicArray(int capacity)
		{
			mass = new T[capacity];
			Capacity = capacity;
		}

		public DynamicArray(IEnumerable<T> items)
		{
			Capacity = items.Count();
			mass = new T[Capacity];
            foreach (var elem in items)
            {
				Add(elem);
            }
		}

		public IEnumerator<T> GetEnumerator()
		{
			foreach (var elem in mass)
				yield return elem;
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
        }

        public T this[int index]
		{
            get
            {
				if (index >= Length)
					throw new IndexOutOfRangeException("Index outside the array");
				return mass[index];
            }
			set
            {
				if (index >= Length)
					throw new IndexOutOfRangeException("Index outside the array");
				else
					mass[index] = value;
            }
		}


        public int Length { get; set; }
		public int Capacity { get; set; }
		public void Add(T item)
		{
			if (Length == Capacity)
            {
				if (Capacity == 0)
					Capacity = 1;
				Capacity *= 2;
				var massTemp = new T[Capacity];
				Array.Copy(mass, massTemp, Length);
				mass = massTemp;
				mass[Length] = item;
			}
			else
				mass[Length] = item;
			Length++;
		}

		public void AddRange(IEnumerable<T> items)
		{
			foreach (var elem in items)
            {
				Add(elem);
            }
		}

		public void Insert(T item, int index)
		{
			var massTemp = CopyElements(index);
			mass[index] = item;
			Length = index + 1;
			foreach (var elem in massTemp)
            {
				Add(elem);
            }
		}

		public bool Remove(T item)
		{
			var indexRemovableElement = -1;
			for (int i = 0; i < Length; i++)
            {
				if (mass[i].ToString() == item.ToString())
                {
					indexRemovableElement = i;
					break;
				}
            }

			if (indexRemovableElement == -1)
				return false;

			var massTemp = CopyElements(indexRemovableElement + 1);
			mass = CopyElements(0, indexRemovableElement);
			Length = indexRemovableElement;

			for (int i = 0; i < massTemp.Length; i++)
            {
				if (i != indexRemovableElement)
					Add(massTemp[i]);
            }
			return true;
		}

		private T[] CopyElements(int indexStart, int indexEnd = -1 )
        {
			var massTemp = new T[Length];
			for (int i = indexStart; i < (indexEnd == -1 ? Length : indexEnd); i++)
			{
				massTemp[i] = mass[i];
			}
			return massTemp;
		}
	}
}